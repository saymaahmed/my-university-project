<!DOCTYPE html>
<html lang="en">
<head>
    <title>My Unidersity Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Department <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="Views/dept_create.php">Save department info</a></li>
                        <li><a href="Views/showall.php">View all department</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">course <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="Views/course_create.php">Save course</a></li>
                        <li><a href="#">View course statics</a></li>
                        <li><a href="#">Unassigned course</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Teacher<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="Views/teacher_create.php">Save Teacher Info</a></li>
                        <li><a href="#">View all department</a></li>
                    </ul>
                </li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="Views/registration.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="Views/login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </div>
</nav>
</body>
</html>