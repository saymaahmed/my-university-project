<?php
include_once ("../vendor/autoload.php");
use App\Department\Department;

$showallobj = new Department();
$alldata = $showallobj->getAllinfo();
?>

<!DOCTYPE html>
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
</head>
<body>
<div class="container">
    <div class="row1">
        <div class="col-lg-8 col-lg-push-2">
            <div class="well well-sm">
                <h4>view All Courses</h4>
            </div>
        </div>
    </div>
    <div class="row2">
        <div class="col-lg-8 col-lg-push-2">
            <div class="panel">
                <div class="table-bordered table-responsive text-center">
<table class="table table-bordered" style="border: 1px solid:#ddd !important;">
    <thead>
        <th>Code</th>
        <th>Name</th>
    </thead>
    <?php foreach ($alldata as $item) {?>
        <tbody>
            <td><?php echo $item['code']?></td>
            <td><?php echo $item['dept_name']?></td>
        </tbody>
    <?php } ?>

</table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>