<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row1">
        <div class="col-lg-8 col-lg-push-3">
            <div class="well well-sm">
                <h4>save department</h4>
            </div>
        </div>
    </div>
    <div class="row2">
        <div class="col-lg-8 col-lg-push-3">
            <div class="well well-lg">
                <form class=form-horizontal action="dept_store.php" method="post">
                    <label>code</label>
                    <input type="text" name="ecode" maxlength="7" minlength="2">
                    <label>Name</label>
                    <input type="text" name="ename">
                    <input type="submit" value="SEND">
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>