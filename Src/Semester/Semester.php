<?php
namespace App\Semester;
include ("../../vendor/autoload.php");
use PDO;
session_start();

class Semester
{
   private $name;

   public function setData($data=""){
       if(!empty($data['text'])){
           $this->name = $data['text'];
       }
       return $this;
   }

   public function store(){
       try {
           $pdo = new PDO('mysql:host=localhost;dbname=university_db', 'root', '');
           $query = 'INSERT INTO semesters(semester_name)
                          VALUES(:cname)';
           $stmt = $pdo->prepare($query);
           $data = [
               ':cname' => $this->name,
           ];
           $status = $stmt->execute($data);
           if ($status) {
               $_SESSION['Message'] = '<h1>Successfully Registered </h1>';
               header('location:../index.php');
           } else {
               echo "<h1>Opps Something wrong</h1>";

           }
       } catch (PDOException $e) {
           echo 'Error: ' . $e->getMessage();
       }
   }


}