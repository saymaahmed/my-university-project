<?php
namespace App\Designation;
include ("../../vendor/autoload.php");
use PDO;
session_start();

class Designation
{
    private $designation;

    public function setData($data=""){
        if(!empty($data['desig'])){
            $this->designation= $data['desig'];
        }
        return $this;
    }
    public function Store(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=university_db', 'root', '');
            $query = 'INSERT INTO designations(title)
                          VALUES(:dtitle)';
            $stmt = $pdo->prepare($query);
            $data = [
                ':dtitle' => $this->designation,
            ];
            $status = $stmt->execute($data);
            if ($status) {
                $_SESSION['Message'] = '<h1>Successfully Registered </h1>';
                header('location:../index.php');
            } else {
                echo "<h1>Opps Something wrong</h1>";

            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

}