<?php
namespace App\Course;
include_once("../../vendor/autoload.php");
use App\Utility\Utility;
use PDO;
session_start();
class Course
{
    private $code;
    private $name;
    private $credit;
    private $description;
    private $deptname;
    private $semsname;

    public function setdata($data=""){
        if(!empty($data['ecode'])){
            $this->code = $data['ecode'];
        }
        if(!empty($data['ename'])){
            $this->name = $data['ename'];
        }
        if(!empty($data['ecredit'])){
            $this->credit = $data['ecredit'];
        }
        if(!empty($data['edescription'])){
            $this->description = $data['edescription'];
        }
        if(!empty($data['dname'])){
            $this->deptname = $data['dname'];
        }
        if(!empty($data['sname'])){
            $this->semsname = $data['sname'];
        }
        return $this;
    }
    public function store(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=university_db', 'root', '');
            $query = 'INSERT INTO courses(code,course_name,credit,description,dept_name,semester_name)
                          VALUES(:ccode,:cname,:ccredit,:cdes,:cdeptname,:csemname)';
            $stmt = $pdo->prepare($query);
            $data = [
                ':ccode' => $this->code,
                ':cname' => $this->name,
                ':ccredit'=> $this->credit,
                ':cdes'=> $this->description,
                ':cdeptname'=> $this->deptname,
                ':csemname'=> $this->semsname,
            ];
            $status = $stmt->execute($data);
            if ($status) {
                $_SESSION['Message'] = '<h1>Successfully Registered </h1>';
                header('location:../index.php');
            } else {
                echo "<h1>Opps Something wrong</h1>";

            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

}