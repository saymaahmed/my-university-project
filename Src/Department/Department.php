<?php
namespace App\Department;
include ("../../vendor/autoload.php");
use App\Utility\Utility;
use PDO;
session_start();

class Department
{
    private $code;
    private $name;


    public function setData($data = "")
    {
        if (!empty($data['ecode'])) {
            $this->code = $data['ecode'];
        }
        if (!empty($data['ename'])) {
            $this->name = $data['ename'];
        }

        return $this;

    }

    public function Store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=university_db', 'root', '');
            $query = 'INSERT INTO departments(code,dept_name)
                          VALUES(:dcode,:dname)';
            $stmt = $pdo->prepare($query);
            $data = [
                ':dcode' => $this->code,
                ':dname' => $this->name,
            ];
            $status = $stmt->execute($data);
            if ($status) {
                $_SESSION['Message'] = '<h1>Successfully Registered </h1>';
                header('location:../index.php');
            } else {
                echo "<h1>Opps Something wrong</h1>";

            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function getAllinfo()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=university_db', 'root', '');
            $query = 'SELECT * FROM `departments`';
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $list = $stmt->fetchAll();
            return $list;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}