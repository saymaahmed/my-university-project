<?php
namespace App\Teacher;
include ("../../vendor/autoload.php");
use PDO;
session_start();
class Teacher
{
    private $name;
    private $address;
    private $email;
    private $contactNo;
    private $creditTaken;
    private $title;
    private $deptName;

    public function setData($data = "")
    {
        if (!empty($data['ename'])) {
            $this->name = $data['ename'];
        }
        if (!empty($data['eaddress'])) {
            $this->address = $data['eaddress'];
        }
        if (!empty($data['eemail'])) {
            $this->email = $data['eemail'];
        }
        if (!empty($data['econtno'])) {
            $this->contactNo = $data['econtno'];
        }
        if (!empty($data['dtitle'])) {
            $this->title = $data['dtitle'];
        }
        if (!empty($data['dname'])) {
            $this->deptName = $data['dname'];
        }
        if (!empty($data['etaken'])) {
            $this->creditTaken = $data['etaken'];
        }

        return $this;
    }
    public function store(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=university_db', 'root', '');
            $query = 'INSERT INTO teachers(teacher_name,address,email,contact_no,
                              designation,departments,credit_taken)
                          VALUES(:tname,:tadd,:temail,:tcont,:tdesig,:tdepart,:tcredit)';
            $stmt = $pdo->prepare($query);
            $data = [
                ':tname' => $this->name,
                ':tadd' => $this->address,
                ':temail'=> $this->email,
                ':tcont'=> $this->contactNo,
                ':tdesig'=>$this->title,
                ':tdepart'=>$this->deptName,
                ':tcredit'=> $this->creditTaken,
            ];
            $status = $stmt->execute($data);
            if ($status) {
                $_SESSION['Message'] = '<h1>Successfully Registered </h1>';
                header('location:../index.php');
            } else {
                echo "<h1>Opps Something wrong</h1>";

            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }
}